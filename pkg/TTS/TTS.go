package TTS

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strings"
)


type TTS struct{
	Language string
}


func New(lang string) *TTS {
	tts := TTS{}
	if len(lang) == 0 {
		log.Printf("Defaulting to English Language")
		tts.Language = "en"
	}
	tts.Language = lang
	return &tts
}


func (tts *TTS) Request(query string) ([]byte, error){
	Baseurl := &url.URL{
		Scheme: "http",
		Host: "translate.google.com",
		Path: "/translate_tts",
	}
	Param := url.Values{}
	Param.Add("ie", "UTF-8")
	Param.Add("total", "1")
	Param.Add("idx", "0")
	Param.Add("testlen",fmt.Sprintf("%d",len(query)))
	Param.Add("client", "tw-ob")
	Param.Add("q", query)
	Param.Add("tl", tts.Language)

	Baseurl.RawQuery = Param.Encode()

	resp, err := http.Get(Baseurl.String())
	buf := new(bytes.Buffer)

	if err != nil{
		return nil, err
	}
	defer resp.Body.Close()

	_, err = io.Copy(buf, resp.Body)
	if err != nil{
		return nil, err
	}
	//log.Printf("\n%d bytes writtern to the buffer",writtenLength)
	return buf.Bytes(), nil
}



func (tts *TTS)Read(query string) ([]byte, error) {
	sentences := strings.Split(query, ".")
	var buffer string
	audioBuffer := new(bytes.Buffer)
	for _, sentence := range sentences{
		// Appending Fullstop after hradings
		sentence = sentence + "."
		lines := strings.Split(sentence, "\n")
		if len(lines) >= 3{
			for i, line := range lines{
				if len(line) > 2 && line == strings.ToUpper(line) {
					lines[i] = line + ". "
					sentence = strings.ReplaceAll(sentence, line, lines[i])
					//fmt.Println("Headings: \n", lines[i])
				}
			}
		}
		
		sentence = strings.ReplaceAll(sentence, "\n", " ")

		if len(sentence) <= 200{
			audioTemp,err := tts.Request(sentence)
			if err != nil{
				return nil,err	
			}
			audioBuffer.Write(audioTemp)
		}else{
			//fmt.Println("SENTENCE 200: ===> \n", sentence)
			for len(sentence) > 0 {
				if len(sentence) < 200{
					audioTemp, err := tts.Request(sentence)
					if err != nil{
						return nil, err
					}
					audioBuffer.Write(audioTemp)
					break
				}
				buffer ,sentence = generateBuffer(sentence) 
				//fmt.Println("Query 200: ===> \n", string(sentence))
				//fmt.Println("Current Buffer TTS", buffer)
				 audioTemp, err := tts.Request(buffer)
				if err != nil{
					return nil, err
				}
				audioBuffer.Write(audioTemp)
			}
		}

	}
	return audioBuffer.Bytes(), nil
}


func generateBuffer(str string)(string, string){
	var buf []byte
	var spaceIndex int
	for  i,c:= range []rune(str){
		if ( i > 200 ){
			break 
		}
		if " " == string(c) {
			spaceIndex = i
		}

		buf = append(buf,byte(c))
	}
	return /*buffer*/string(buf[0:spaceIndex]), /*backlog*/string([]rune(str)[spaceIndex:])
}
