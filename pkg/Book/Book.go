package Book

import (
	"gitlab.com/devi0usm0nk/ReadToMe/pkg/TTS"
	"os"
	"log"
	"strings"
	"bytes"
	"fmt"
)

type chapter struct{
	Name string
	PageNo int
}
type BookObj struct{
	Pages []string
	Chapters []chapter
	FilePath string
	OutputDir string
	Name string
}

func	InitBook(filePath string, OutputDir string, name string) BookObj{
	book := BookObj{}
	book.OutputDir = OutputDir
	book.Name = name
	fileBytes, err := os.ReadFile(filePath)
	if err != nil{
		log.Printf("\nError occured while reading the file: \n%e",err)
	}
	book.FilePath = filePath

	pageDelimitor := ""
	bookTmp := string(fileBytes)
	bookTmp = strings.ReplaceAll(bookTmp, "’", "'")
	bookTmp = strings.ReplaceAll(bookTmp, "“", string('"'))
	bookTmp = strings.ReplaceAll(bookTmp, "”", string('"'))

	pages := strings.Split(bookTmp, pageDelimitor)


	for _, page := range pages{
		size := len(page)
		if size != 0{
			page = strings.ReplaceAll(page, "\n\n", ".\n\n")
			book.Pages = append(book.Pages, page)
		}
	}
	book.initChapters()
	return book
}

func (book *BookObj) initChapters(){
	for pageNo, page := range book.Pages{
		firstLine := strings.Split(page,"\n")[0]
		if strings.ToUpper(firstLine) == firstLine{
			book.Chapters = append(book.Chapters, chapter{Name: firstLine, PageNo: pageNo})
			
		}
	}
}

func (book *BookObj) ReadPages(pages []string)([]byte, error){
	var audioBuffers bytes.Buffer
	tts := TTS.New("en")

	if len(pages) == 0{
		pages = book.Pages
	}

	for pageNo, page := range pages{
		fmt.Println("PageNo: ",pageNo, " \n===>",page)
		audioTemp, err := tts.Read(page)
		if err != nil{
			return nil, err
		}
		audioBuffers.Write(audioTemp)
	}
	return audioBuffers.Bytes(), nil
}

func (book *BookObj) ReadBook()error{
	audioBuffer, err := book.ReadPages([]string{})
	if err != nil{
		return err
	}

	permissions := os.FileMode(int(0755))
	permissions.IsDir()

	BasePath := book.OutputDir+"/"+book.Name+"/"

	err = os.MkdirAll(BasePath, permissions)
	if err !=nil{
		return err
	}

	f, err := os.Create(BasePath+book.Name+".mp3")
		if err != nil{
			return err
		}
		defer f.Close()
		f.Write(audioBuffer)
	return nil
}

func (book *BookObj) ReadChapter (chapterNo int) []byte{
	chapter := book.Chapters[chapterNo]
	if chapterNo == len(book.Chapters)-1{
		audioBuffer, err := book.ReadPages(book.Pages[chapter.PageNo:len(book.Pages)])
		if err != nil{
			log.Panicln("Error Occured:",err)
		}
		return audioBuffer
	}
	audioBuffer, err := book.ReadPages(book.Pages[chapter.PageNo:book.Chapters[chapterNo+1].PageNo])
	if err != nil{
		log.Panicln("Error Occured:",err)
	}
	return audioBuffer
	
}

func (book *BookObj) ReadChapters()(error){
	permissions := os.FileMode(int(0755))
	permissions.IsDir()

	BasePath := book.OutputDir+"/"+book.Name+"/"

	err := os.MkdirAll(BasePath, permissions)
	if err !=nil{
		return err
	}

	for i:=0; i<len(book.Chapters);i++{
		audioBuffer := book.ReadChapter(i)
		f, err := os.Create(BasePath+fmt.Sprint(i,". ")+book.Chapters[i].Name+".mp3")
		if err != nil{
			return err
		}
		defer f.Close()
		f.Write(audioBuffer)
	}
	return nil
}
