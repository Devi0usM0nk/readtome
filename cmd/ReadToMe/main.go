package main

import(
	"gitlab.com/devi0usm0nk/ReadToMe/pkg/Book"
	"log"
	"flag"
)

func main(){
	var (
		outputDir string
		bookName string
		bookPath string
		readMode string
	)

	flag.StringVar(&outputDir, "output", "output", "Specify a directory to store output")
	flag.StringVar(&bookName, "name", "Untitled_Book", "Specify the name of the Book")
	flag.StringVar(&bookPath, "path", "", "Specify the path to Book's text file")
	flag.StringVar(&readMode, "mode", "", "Specify one of the two modes:\n\t 'chapter': Gives audio o/p according to chapters \n\t 'book': Gives single audio containing whole book")
	flag.Parse()

	if bookPath == ""{
		log.Fatalln("Specify the path to Book's text file")
	}

	// Initializing BookObj
	book := Book.InitBook(bookPath,outputDir,bookName)

	if readMode == ""{
		log.Fatalln("Invalid read mode!!!")
	}else if readMode == "chapter"{
		err := book.ReadChapters()
		if err != nil{
			log.Println("Error occured while reading chapters:\n\t",err)
		}		
	}else if readMode == "book"{
		err := book.ReadBook()
		if err != nil{
			log.Println("Error occured while reading book:\n\t",err)
		}		

	}else {
		log.Fatalln("Invalid read mode!!!")
	}
}
