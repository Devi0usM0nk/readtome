# ReadToMe
A tool whose goal is to convert a pdf to mp3.

> NOTE: DO NOT USE FOR COMMERCIAL PURPOSES

## What's Working
* Packages Book and TTS and complete where TTS is used for Text-To-Speech and Book to manage data read from Books.
* Works on files converted from pdf to text using pdftotext found in poppler-utils 

> NOTE: DO NOT USE FOR COMMERCIAL PURPOSES

## Todo
* Add Proper Comments for readability
* Implement audio playing capability using [go-mp3](https://github.com/hajimehoshi/go-mp3)
* Build a interface poppler or poppler-util's pdftotext application
* Add translate functionality 
* make an gui app to manage audiobooks

> NOTE: DO NOT USE FOR COMMERCIAL PURPOSES

